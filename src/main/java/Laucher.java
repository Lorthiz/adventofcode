import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ressigningparts.ConfigurationFile;
import ressigningparts.api.AdventOfCodeDTO;
import ressigningparts.api.AdventOfCodeService;

@Controller
@EnableAutoConfiguration
public class Laucher {


    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello world!";
    }

    public static void main(String[] args) {
        SpringApplication.run(Laucher.class, args);
    }

    @RequestMapping("/day")
    @ResponseBody
    AdventOfCodeDTO dayOfAdvent(@RequestParam(value = "input0", required = true) String file) {

        AnnotationConfigApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(ConfigurationFile.class);

        AdventOfCodeService aocService = annotationConfigApplicationContext.getBean("aocService", AdventOfCodeService.class);

        return aocService.getSolutionFormFile(file);
    }

    @RequestMapping("/dayInput")
    @ResponseBody
    AdventOfCodeDTO dayOfAdventFromInput(@RequestParam(value = "input0", required = true) String input,
                                         @RequestParam(value = "input1", required = true) String file) {

        AnnotationConfigApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(ConfigurationFile.class);

        AdventOfCodeService aocService = annotationConfigApplicationContext.getBean("aocService", AdventOfCodeService.class);

        return aocService.getSolutionForInput(input, file);
    }
}
