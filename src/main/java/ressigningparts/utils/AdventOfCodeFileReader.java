package ressigningparts.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AdventOfCodeFileReader {
    private BufferedReader br;

    public AdventOfCodeFileReader(String filename) {
        InputStream in = getClass().getResourceAsStream("../../" + filename);
        br = new BufferedReader(new InputStreamReader(in));
    }

    public List<String> getFileContent() {
        String strLine;
        ArrayList<String> content = new ArrayList<>();

        try {
            while ((strLine = br.readLine()) != null) {
                content.add(strLine);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }
}
