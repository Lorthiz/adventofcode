package ressigningparts.impl;

import ressigningparts.api.AdventOfCode;
import ressigningparts.utils.AdventOfCodeFileReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractAdventOfCode implements AdventOfCode {

    protected List<String> input;
    protected String firstPart;
    protected String secondPart;

    public AbstractAdventOfCode(){
        input = new ArrayList<>();
        firstPart = "";
        secondPart = "";
    }

    @Override
    public String getFirstPart() {
        return firstPart;
    }

    @Override
    public String getSecondPart() {
        return secondPart;
    }

    @Override
    public void setInputFromFile(String filePath){
        input = new AdventOfCodeFileReader(filePath).getFileContent();
    }

    @Override
    public void setInput(String input){
        this.input = Arrays.asList(input.split("\n"));
    }

    protected void clearData(){
        firstPart = "";
        secondPart = "";
    }

}
