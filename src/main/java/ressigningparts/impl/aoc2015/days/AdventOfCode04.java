package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component("2015/AdventOfCode04")
public class AdventOfCode04 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        firstPart = String.valueOf(crackPasswordThisLong(5, input.get(0)));
        secondPart = String.valueOf(crackPasswordThisLong(6, input.get(0)));
    }

    private String crack(String cracked, MessageDigest cracker) {
        byte[] bytes = cracker.digest(cracked.getBytes());
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private int crackPasswordThisLong(int length, String password) {
        int crackedPass = 0;
        String zeroCode = zeroCodeOfLength(length);
        MessageDigest cracker = initializeCracker();
        while (true) {
            String cracked = crack(password + Integer.toString(crackedPass++), cracker);
            if (cracked.substring(0, length).equals(zeroCode)) {
                return --crackedPass;
            }
        }
    }

    private MessageDigest initializeCracker() {
        MessageDigest cracker = null;
        try {
            cracker = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return cracker;
    }

    private String zeroCodeOfLength(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; ++i) {
            stringBuilder.append('0');
        }
        return stringBuilder.toString();
    }
}
