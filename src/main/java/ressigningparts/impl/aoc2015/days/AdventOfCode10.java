package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;

@Component("2015/AdventOfCode10")
public class AdventOfCode10 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        firstPart = String.valueOf(prepareSolution(40, input.get(0)).length());
        secondPart = String.valueOf(prepareSolution(50, input.get(0)).length());
    }

    private String processInput(String input){
        StringBuilder output = new StringBuilder();
        for(int i = 0; i < input.length();){
            char actualChar = input.charAt(i);
            int count = 1;
            while(++i < input.length() && input.charAt(i) == actualChar){
                count++;
            }
            output.append(count).append(actualChar);
        }
        return output.toString();
    }

    private String prepareSolution(int x, String input){
        for(int i = 0; i < x; ++i){
            input = processInput(input);
        }
        return input;
    }
}
