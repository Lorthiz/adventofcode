package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;

@Component("2015/AdventOfCode05")
public class AdventOfCode05 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        int niceWords = 0;
        int niceWordsNewWay = 0;
        for (String word : input) {
            if (isNice(word)) {
                ++niceWords;
            }
            if (isNiceNewWay(word)) {
                ++niceWordsNewWay;
            }
        }
        firstPart = String.valueOf(niceWords);
        secondPart = String.valueOf(niceWordsNewWay);
    }

    private boolean isNice(String word) {
        return !(containsBadSubstrings(word) || !containsDoubleLetters(word) || countVovels(word) < 3);
    }

    private boolean isNiceNewWay(String word) {
        boolean firstCondition = false;
        boolean secondCondition = false;
        String substring = "aa";
        while (substring != null && (!firstCondition || !secondCondition)) {
            if (!firstCondition && containsSubstringAtLeastTwice(word, substring)) {
                firstCondition = true;
            }
            if (!secondCondition && word.contains(addFirstCharAtTheEnd(substring))) {
                secondCondition = true;
            }
            substring = incrementSubstring(substring);
        }
        return firstCondition && secondCondition;
    }

    private int countVovels(String word) {
        int vovels = 0;
        for (int i = 0; i < word.length(); ++i) {
            if (isVovel(word.charAt(i))) {
                ++vovels;
            }
        }
        return vovels;
    }

    private boolean containsDoubleLetters(String word) {
        for (int i = 'a'; i <= 'z'; ++i) {
            StringBuilder sb = new StringBuilder();
            sb.append((char) i).append((char) i);
            if (word.contains(sb.toString())) {
                return true;
            }
        }
        return false;
    }

    private boolean containsBadSubstrings(String word) {
        return word.contains("ab") || word.contains("cd") ||
                word.contains("pq") || word.contains("xy");
    }

    private boolean isVovel(char c) {
        return c == 'a' || c == 'e' || c == 'i' ||
                c == 'o' || c == 'u';
    }

    private boolean containsSubstringAtLeastTwice(String word, String substring) {
        String splited[] = word.split(substring);
        int length = 0;
        for (String aSplited : splited) {
            length += aSplited.length();
        }
        return length <= word.length() - 4;
    }

    private String incrementSubstring(String substring) {
        char[] chars = substring.toCharArray();
        ++chars[1];
        if (chars[1] > 'z') {
            chars[1] = 'a';
            ++chars[0];
        }
        if (chars[0] > 'z') {
            return null;
        }
        return new String(chars);
    }

    private String addFirstCharAtTheEnd(String substring) {
        return substring + substring.charAt(0);
    }
}
