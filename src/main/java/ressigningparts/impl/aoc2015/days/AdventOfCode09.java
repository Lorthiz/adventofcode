package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;
import ressigningparts.impl.aoc2015.helpers.City;

@Component("2015/AdventOfCode09")
public class AdventOfCode09 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        City.clear();
        for (String s : input) {
            City.createCity(s);
        }

        int shortestRoute = 0;
        for (String[] cities : City.permutationOfAllCities()) {
            int distance = City.getDistance(cities);
            if (shortestRoute == 0 || shortestRoute > distance) {
                shortestRoute = distance;
            }
        }
        firstPart = String.valueOf(shortestRoute);

        int longestRoute = 0;
        for (String[] cities : City.permutationOfAllCities()) {
            int distance = City.getDistance(cities);
            if (longestRoute == 0 || longestRoute < distance) {
                longestRoute = distance;
            }
        }
        secondPart = String.valueOf(longestRoute);
    }
}
