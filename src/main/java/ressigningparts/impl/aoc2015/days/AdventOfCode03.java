package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;
import ressigningparts.impl.aoc2015.helpers.Position;
import ressigningparts.impl.aoc2015.helpers.Santa;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Component("2015/AdventOfCode03")
public class AdventOfCode03 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        firstPart = String.valueOf(parseHouses(1));
        secondPart = String.valueOf(parseHouses(2));
    }

    private int parseHouses(int numberOfSantas) {
        List<Santa> santas = createSantas(numberOfSantas);
        ArrayList<Position> houses = new ArrayList<>();
        houses.add(new Position(0,0));
        int numberOfTurns = input.get(0).length();

        for (int i = 0; i < numberOfTurns; ++i) {
            switchHouse(thisTurnSanta(santas, i), input.get(0).charAt(i), houses);
        }

        return houses.size();
    }

    private void switchHouse(Santa santa, char c, List<Position> houses) {
        Position house = new Position(0, 0);
        switch (c) {
            case '<':
                house = new Position(santa.moveWest(), santa.getY());
                break;
            case '>':
                house = new Position(santa.moveEast(), santa.getY());
                break;
            case 'v':
                house = new Position(santa.getX(), santa.moveSouth());
                break;
            case '^':
                house = new Position(santa.getX(), santa.moveNorth());
                break;
        }
        if (!houses.contains(house)) {
            houses.add(house);
        }
    }

    private List<Santa> createSantas(int numberOfSantas) {
        List<Santa> santas = new ArrayList<>();
        IntStream.range(0, numberOfSantas).forEach(i -> santas.add(new Santa()));
        return santas;
    }

    private Santa thisTurnSanta(List<Santa> santas, int moveNumber) {
        return santas.get(moveNumber % santas.size());
    }
}
