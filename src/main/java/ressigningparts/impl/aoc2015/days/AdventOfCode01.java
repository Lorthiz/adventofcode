package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;

@Component("2015/AdventOfCode01")
public class AdventOfCode01 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        int floorNumber = 0;
        boolean beenInBasement = false;
        for (int i = 0; i < input.get(0).length(); ++i) {
            if (isGoingUp(i)) {
                ++floorNumber;
            } else if (isGoingDown(i)) {
                --floorNumber;
                beenInBasement = beenInBasementBefore(beenInBasement, i, floorNumber);
            }
        }
        firstPart = String.valueOf(floorNumber);
    }

    private boolean isGoingDown(int i) {
        return input.get(0).charAt(i) == ')';
    }

    private boolean isGoingUp(int i) {
        return input.get(0).charAt(i) == '(';
    }

    private boolean beenInBasementBefore(boolean beenInBasement, int moves, int floorNumber) {
        if (!beenInBasement && floorNumber == -1) {
            secondPart = String.valueOf(moves + 1);
            return true;
        }
        return beenInBasement;
    }
}
