package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.aoc2015.helpers.Person;
import ressigningparts.impl.AbstractAdventOfCode;

import java.util.*;

@Component("2015/AdventOfCode13")
public class AdventOfCode13 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        firstPart = calculateFirstPart();
        secondPart = calculateSecondPart();
    }

    private String calculateFirstPart() {
        LinkedHashMap<String,Person> nameToRelations = new LinkedHashMap<>();
        input.forEach(sentence -> {
            String name = sentence.split(" ")[0];
            Person person = new Person(name);
            nameToRelations.putIfAbsent(name, person);
            nameToRelations.get(name).addRelation(sentence);
        });

        return nameToRelations.toString();
    }

    private String calculateSecondPart() {
        return null;
    }

    private void debug(LinkedHashMap<String,Person> nameToRelations){
        nameToRelations.forEach((v, k) -> System.out.println(k));
//        System.out.println(nameToRelations.toString());
    }
}
