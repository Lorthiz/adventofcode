package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;
import ressigningparts.impl.aoc2015.helpers.Command;
import ressigningparts.impl.aoc2015.helpers.Light;

import java.util.ArrayList;
@Component("2015/AdventOfCode06")
public class AdventOfCode06 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        ArrayList<Command> commands;
        Light[][] lights = initLights(1000);
        commands = new ArrayList<>();
        for(String command: input){
            command = command.replaceAll(" through ", " ");
            command = command.replaceAll("turn ", "");
            commands.add(new Command(command));
        }
        for(Command c: commands){
            executeCommand(c, lights);
        }
        firstPart = String.valueOf(countLights(lights));
        secondPart = String.valueOf(countBrightness(lights));
    }

    private Light[][] initLights(int size){
        Light[][] lights = new Light[size][size];
        for(int i = 0; i < size; ++i){
            for(int j = 0; j < size; ++j){
                lights[i][j] = new Light();
            }
        }
        return lights;
    }

    private void executeCommand(Command command, Light[][] lights){
        switch(command.getType()){
            case ON:
                on(command.getStartX(), command.getStartY(), command.getEndX(), command.getEndY(), lights);
                break;
            case OFF:
                off(command.getStartX(), command.getStartY(), command.getEndX(), command.getEndY(), lights);
                break;
            case TOGGLE:
                toggle(command.getStartX(), command.getStartY(), command.getEndX(), command.getEndY(), lights);
                break;
        }
    }

    private void on(int startX, int startY, int endX, int endY, Light[][] lights){
        for(int i = startX; i <= endX; ++i){
            for(int j = startY; j <= endY; ++j){
                lights[j][i].turnOn();
            }
        }
    }

    private void off(int startX, int startY, int endX, int endY, Light[][] lights){
        for(int i = startX; i <= endX; ++i){
            for(int j = startY; j <= endY; ++j){
                lights[j][i].turnOff();
            }
        }
    }

    private void toggle(int startX, int startY, int endX, int endY, Light[][] lights){
        for(int i = startX; i <= endX; ++i){
            for(int j = startY; j <= endY; ++j){
                lights[j][i].toggle();
            }
        }
    }

    private int countLights(Light[][] lights){
        int counter = 0;
        for (Light[] light : lights) {
            for (int j = 0; j < lights.length; ++j) {
                if (light[j].getStatus()) {
                    ++counter;
                }
            }
        }
        return counter;
    }

    private int countBrightness(Light[][] lights){
        int counter = 0;
        for (Light[] light : lights) {
            for (int j = 0; j < lights.length; ++j) {
                counter += light[j].getBrightness();
            }
        }
        return counter;
    }
}
