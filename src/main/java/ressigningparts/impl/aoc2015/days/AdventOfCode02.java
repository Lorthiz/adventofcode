package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;
import ressigningparts.impl.aoc2015.helpers.Present;

@Component("2015/AdventOfCode02")
public class AdventOfCode02 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        int overAllSize = 0;
        int overAllRibbon = 0;
        for(String line: input){
            Present present = Present.createPresent(line);
            overAllSize += present.getSurface();
            overAllRibbon += present.getRibbon();
        }
        firstPart = String.valueOf(overAllSize);
        secondPart = String.valueOf(overAllRibbon);
    }
}
