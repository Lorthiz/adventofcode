package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;
import ressigningparts.impl.aoc2015.helpers.Password;

@Component("2015/AdventOfCode11")
public class AdventOfCode11 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        Password password = new Password(input.get(0));
        firstPart = generateNewAndReturnValue(password);
        secondPart = generateNewAndReturnValue(password);
    }

    private String generateNewAndReturnValue(Password password){
        password.generate();
        return password.getPassword();
    }

}
