package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;
import ressigningparts.impl.aoc2015.helpers.Signal;
import ressigningparts.impl.aoc2015.helpers.SignalCommand;

import java.util.ArrayList;
import java.util.HashMap;

@Component("2015/AdventOfCode07")
public class AdventOfCode07 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        HashMap<String, Signal> signals = new HashMap<>();
        ArrayList<SignalCommand> commands = new ArrayList<>();
        for (String command : input) {
            commands.add(new SignalCommand(command));
        }
        while (commands.size() > 0) {
            commands.removeIf(command -> command.execute(signals));
        }
        firstPart = signals.get("a").toString();
        for (String command : input) {
            commands.add(new SignalCommand(command));
        }
        for (SignalCommand command : commands) {
            if (command.getOutput().equals("b")) {
                command.setFirstInput(signals.get("a").toString());
                break;
            }
        }
        signals.clear();
        while (commands.size() > 0) {
            commands.removeIf(command -> command.execute(signals));
        }
        secondPart = signals.get("a").toString();

    }
}
