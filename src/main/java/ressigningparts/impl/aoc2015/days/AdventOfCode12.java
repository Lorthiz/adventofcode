package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component("2015/AdventOfCode12")
public class AdventOfCode12 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        firstPart = calculateFirstPart();
        secondPart = calculateSecondPart();
    }

    private String calculateFirstPart() {
        String[] workingInput = input.get(0)
                .replaceAll("[\\:\\]\\{\\}\"\\[]", ",")
                .replaceAll(",,", ",")
                .split(",");
        List<String> inputs = new ArrayList<>(Arrays.asList(workingInput));
        int firstSum = 0;
        inputs = inputs.stream()
                .filter(s -> s.matches("-?\\d+(\\.\\d+)?"))
                .collect(Collectors.toList());
        for (String s : inputs) {
            firstSum += Integer.parseInt(s);
        }
        return String.valueOf(firstSum);
    }

    private String calculateSecondPart() {
        return null;
    }
}
