package ressigningparts.impl.aoc2015.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.AbstractAdventOfCode;

@Component("2015/AdventOfCode08")
public class AdventOfCode08 extends AbstractAdventOfCode {

    @Override
    public void calculate() {

        int charactersInCode = 0;
        int charactersDecoded = 0;

        for (String s : input) {
            charactersInCode += s.length();
            s = s.replaceAll("\\\\x[0-9a-f]{2}", "#").replace("\\\\", "@").replace("\\\"", "@").replace("\"", "");
            charactersDecoded += s.length();
        }
        firstPart = String.valueOf(charactersInCode - charactersDecoded);

        charactersInCode = 0;
        int charactersEncoded = 0;

        for (String s : input) {
            charactersInCode += s.length();
            s = s.replaceAll("\\\\x[0-9a-f]{2}", "#####").replace("\\\\", "@@@@").replace("\\\"", "@@@@").replace("\"", "@@@");
            charactersEncoded += s.length();
        }
        secondPart = String.valueOf(charactersEncoded - charactersInCode);
    }
}
