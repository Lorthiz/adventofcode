package ressigningparts.impl.aoc2015.helpers;

import java.util.HashMap;

public class SignalCommand {
    private String input1, input2;
    private CommandType type;
    private String output;

    public SignalCommand(String s){
        String[] command = s.split(" ");
        if(command.length == 3){
            input1 = command[0];
            input2 = null;
            type = CommandType.ASSIGN;
            output = command[2];
        } else if(command.length == 4) {
            input1 = command[1];
            input2 = null;
            type = CommandType.NOT;
            output = command[3];
        } else {
            input1 = command[0];
            input2 = command[2];
            switch(command[1]){
                case "AND":
                    type = CommandType.AND;
                    break;
                case "OR":
                    type = CommandType.OR;
                    break;
                case "LSHIFT":
                    type = CommandType.LSHIFT;
                    break;
                case "RSHIFT":
                    type = CommandType.RSHIFT;
                    break;
            }
            output = command[4];
        }
    }

    public String getOutput(){
        return output;
    }

    public void setFirstInput(String input){
        this.input1 = input;
    }

    public boolean execute(HashMap<String, Signal> signals){
        switch (type){
            case AND:
                return ADD(signals);
            case OR:
                return OR(signals);
            case NOT:
                return NOT(signals);
            case LSHIFT:
                return LSHIFT(signals);
            case RSHIFT:
                return RSHIFT(signals);
            case ASSIGN:
                return ASSIGN(signals);
        }
        return false;
    }

    private boolean ADD(HashMap<String, Signal> signals){
        Signal signal1 = checkAndAssignSignal(input1, signals);
        Signal signal2 = checkAndAssignSignal(input2, signals);

        if(signal1 == null || signal2 == null){ return false; }

        signals.put(output, signal1.AND(signal2));
        return true;
    }

    private boolean OR(HashMap<String, Signal> signals){
        Signal signal1 = checkAndAssignSignal(input1, signals);
        Signal signal2 = checkAndAssignSignal(input2, signals);

        if(signal1 == null || signal2 == null){ return false; }

        signals.put(output, signal1.OR(signal2));
        return true;
    }

    private boolean NOT(HashMap<String, Signal> signals){
        Signal signal = checkAndAssignSignal(input1, signals);

        if(signal == null){ return false; }

        signals.put(output, signal.NOT());
        return true;
    }

    private boolean LSHIFT(HashMap<String, Signal> signals){
        Signal signal1 = checkAndAssignSignal(input1, signals);
        Signal signal2 = checkAndAssignSignal(input2, signals);

        if(signal1 == null){ return false; }

        signals.put(output, signal1.LSHIFT(signal2.toInt()));
        return true;
    }

    private boolean RSHIFT(HashMap<String, Signal> signals){
        Signal signal1 = checkAndAssignSignal(input1, signals);
        Signal signal2 = checkAndAssignSignal(input2, signals);

        if(signal1 == null){ return false; }

        signals.put(output, signal1.RSHIFT(signal2.toInt()));
        return true;
    }

    private boolean ASSIGN(HashMap<String, Signal> signals){
        Signal signal = checkAndAssignSignal(input1, signals);

        if(signal == null){ return false; }

        signals.put(output, signal);
        return true;
    }

    public String toString(){
        return input1 + " " + type.toString() + " " + input2 + " -> " + output;
    }

    private Signal checkAndAssignSignal(String input, HashMap<String, Signal> signals){
        Signal signal;
        if(input.matches("\\d+")){
            signal = new Signal(Integer.parseInt(input));
        } else {
            signal = signals.get(input);
        }
        return signal;
    }

}
