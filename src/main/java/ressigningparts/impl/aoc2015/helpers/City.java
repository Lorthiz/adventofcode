package ressigningparts.impl.aoc2015.helpers;

import java.util.*;

public class City {
    private static final Map<String, City> Cities = new HashMap<>();
    private String name;
    private Map<String, Integer> otherCitiesDistances = new HashMap<>();

    private City(String name) {
        this.name = name;
        Cities.put(name, this);
    }

    public static void clear(){
        Cities.clear();
    }

    public static void createCity(String input){
        String[] s;
        s = input.replace(" to ", " ").replace(" = ", " ").split(" ");
        if(!Cities.containsKey(s[0])) {
            new City(s[0]);
        }
        Cities.get(s[0]).otherCitiesDistances.put(s[1], Integer.valueOf(s[2]));

        if(!Cities.containsKey(s[1])) {
            new City(s[1]);
        }
        Cities.get(s[1]).otherCitiesDistances.put(s[0], Integer.valueOf(s[2]));
    }

    public static void showCities(){
        for(City city: Cities.values()){
            System.out.println("This sCity is: " + city.name);
            for(String name: city.otherCitiesDistances.keySet()){
                System.out.println("\t-> " + name +  " -> " + city.otherCitiesDistances.get(name) + "km");
            }
        }
    }

    public static String[] getAvaliableCities(){
        return Cities.keySet().toArray(new String[Cities.size()]);
    }

    public static ArrayList<String[]> permutationOfAllCities(){
        ArrayList<String[]> permutations = new ArrayList<>();
        permutate(Cities.size(), getAvaliableCities(), permutations);
        return permutations;
    }

    public static int getDistance(String[] cities){
        int distance = 0;
        String previousCity;
        String actualCity = "";
        for(String city: cities){
            previousCity = actualCity;
            actualCity = city;
            if(previousCity.equals("")){
                continue;
            }
            distance += Cities.get(previousCity).otherCitiesDistances.get(actualCity);
        }
        return distance;
    }

    private static void permutate(int n, String[] cities, ArrayList<String[]> permutations){
        if(n == 1){
            permutations.add(cities.clone());
        } else {
            for(int i = 0; i < n; ++i){
                permutate(n - 1, cities, permutations);
                int j = (n % 2)==0 ? i : 0;
                String s = cities[j];
                cities[j] = cities[n - 1];
                cities[n - 1] = s;
            }
        }
    }
}
