package ressigningparts.impl.aoc2015.helpers;

public class Santa extends Position {

    public Santa() {
        super(0, 0);
    }

    public int moveEast(){
        return ++x;
    }

    public int moveWest(){
        return --x;
    }

    public int moveNorth(){
        return ++y;
    }

    public int moveSouth(){
        return --y;
    }
}
