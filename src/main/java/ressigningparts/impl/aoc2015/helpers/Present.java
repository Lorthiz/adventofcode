package ressigningparts.impl.aoc2015.helpers;

import java.util.ArrayList;
import java.util.Collections;

public class Present {
    private static int BIG_SIDE = 0;
    private static int BIG_MEASUREMENT = 0;
    private static int MEDIUM_MEASUREMENT = 1;
    private int length;
    private int width;
    private int height;
    private int surface;
    private int ribbon;
    private ArrayList<Integer> sidesAreas = new ArrayList<>();
    private ArrayList<Integer> measurement = new ArrayList<>();

    private Present(String values) {
        String valuesParsed[] = values.split("x");
        this.length = Integer.parseInt(valuesParsed[0]);
        this.width = Integer.parseInt(valuesParsed[1]);
        this.height = Integer.parseInt(valuesParsed[2]);
        calculatePresentParameters();
    }

    public static Present createPresent(String values) {
        return new Present(values);
    }

    private void calculatePresentParameters() {
        calculateAndSortSidesAreas();
        calculateAndSortMeasurements();
        calculateSurface();
        calculateRibbon();
    }

    private void calculateAndSortSidesAreas() {
        sidesAreas.clear();
        sidesAreas.add(length * width);
        sidesAreas.add(length * height);
        sidesAreas.add(width * height);
        Collections.sort(sidesAreas);
    }

    private void calculateAndSortMeasurements() {
        measurement.clear();
        measurement.add(length);
        measurement.add(width);
        measurement.add(height);
        Collections.sort(measurement);
    }

    private void calculateSurface() {
        surface = 2 * length * width + 2 * length * height + 2 * width * height;
        surface += sidesAreas.get(BIG_SIDE);
    }

    private void calculateRibbon() {
        ribbon = 2 * measurement.get(BIG_MEASUREMENT)
                + 2 * measurement.get(MEDIUM_MEASUREMENT)
                + length * width * height;
    }

    public int getSurface() {
        return surface;
    }

    public int getRibbon() {
        return ribbon;
    }
}


