package ressigningparts.impl.aoc2015.helpers;

public class Position {
    protected int x;
    protected int y;

    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj){
        if(obj == this) return true;
        if(obj == null) return false;
        Position house = (Position) obj;
        return x == house.x && y == house.y;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }
}
