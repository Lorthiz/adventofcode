package ressigningparts.impl.aoc2015.helpers;

import java.util.stream.IntStream;

public class Password {

    private static final char LETTER_A = 97;
    private static final char LETTER_X = 120;
    private static final char LETTER_Z = 122;
    private static final char OVERFLOW = 123;

    private String password;

    public Password(String password) {
        this.password = password;
    }

    public void generate() {
        do {
            incrementPassword();
        } while (!isCorrect());
    }

    public String getPassword() {
        return password;
    }

    private void incrementPassword() {
        char[] passwordValues = password.toCharArray();
        for (int i = passwordValues.length - 1; i >= 0; --i) {
            ++passwordValues[i];
            if (passwordValues[i] == OVERFLOW) {
                passwordValues[i] = LETTER_A;
            } else {
                break;
            }
        }
        password = String.valueOf(passwordValues);
    }

    private boolean isCorrect() {
        return firstRequirement() && secondRequirement() && thirdRequirement();
    }

    private boolean firstRequirement() {
        return IntStream.rangeClosed(LETTER_A, LETTER_X)
                .anyMatch(i -> {
                    String threeNextLetters = "" + (char) i + (char) (i + 1) + (char) (i + 2);
                    return password.contains(threeNextLetters);
                });
    }

    private boolean secondRequirement() {
        return !password.contains("i") && !password.contains("o") && !password.contains("l");
    }

    private boolean thirdRequirement() {
        return IntStream.rangeClosed(LETTER_A, LETTER_Z).filter(i -> {
            String doubleLetter = "" + (char) i + (char) (i);
            return password.contains(doubleLetter);
        }).count() > 1;
    }
}
