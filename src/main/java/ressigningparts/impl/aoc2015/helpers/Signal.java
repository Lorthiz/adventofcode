package ressigningparts.impl.aoc2015.helpers;

public class Signal {

    private char signal;

    public Signal(int num) {
        if (num > 65535) {
            this.signal = 65535;
        } else if (num < 0) {
            this.signal = 0;
        } else {
            this.signal = (char) num;
        }
    }

    public Signal NOT() {
        char output = (char) ((char) 65535 - signal);
        return new Signal(output);
    }

    public Signal AND(Signal signal) {
        char output = (char) (this.signal & signal.toInt());
        return new Signal(output);
    }

    public Signal OR(Signal signal) {
        char output = (char) (this.signal | signal.toInt());
        return new Signal(output);
    }

    public Signal LSHIFT(int num) {
        char output = (char) (signal << num);
        return new Signal(output);
    }

    public Signal RSHIFT(int num) {
        char output = (char) (signal >> num);
        return new Signal(output);
    }

    public String toString() {
        return Integer.toString(signal);
    }

    public int toInt() {
        return signal;
    }

}
