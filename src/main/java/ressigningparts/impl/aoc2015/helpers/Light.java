package ressigningparts.impl.aoc2015.helpers;

public class Light {

    private boolean status;
    private int brightness;

    public Light(){
        this.status = false;
        brightness = 0;
    }

    public void turnOn(){
        status = true;
        ++brightness;
    }

    public void turnOff(){
        status = false;
        --brightness;
        if(brightness < 0){
            brightness = 0;
        }
    }

    public void toggle(){
        status = !status;
        brightness += 2;
    }

    public boolean getStatus(){
        return status;
    }

    public int getBrightness(){
        return brightness;
    }
}
