package ressigningparts.impl.aoc2015.helpers;

public class Command {
    private CommandTypes type;
    private Point start, end;

    public Command(String command){
        String[] commandParts = command.split(" ");
        setType(commandParts[0]);
        String[] point = commandParts[1].split(",");
        start = new Point(Integer.valueOf(point[0]),Integer.valueOf(point[1]));
        point = commandParts[2].split(",");
        end = new Point(Integer.valueOf(point[0]),Integer.valueOf(point[1]));
    }

    private void setType(String type) {
        switch (type){
            case "on":
                this.type = CommandTypes.ON;
                break;
            case "off":
                this.type = CommandTypes.OFF;
                break;
            case "toggle":
                this.type = CommandTypes.TOGGLE;
                break;
        }
    }

    public CommandTypes getType() {
        return type;
    }

    public int getStartX(){
        return start.getX();
    }

    public int getStartY(){
        return start.getY();
    }

    public int getEndX(){
        return end.getX();
    }

    public int getEndY(){
        return end.getY();
    }

    public String toString(){
        return type.toString() + " | s(" + start.getX() + "," + start.getY() + ") | e("
                + end.getX() + "," + end.getY() + ")";
    }
}
