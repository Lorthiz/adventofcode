package ressigningparts.impl.aoc2015.helpers;

import java.util.HashMap;

public class Person {

    private String name;
    private HashMap<String, Integer> relations = new HashMap<>();

    public Person(String name) {
        this.name = name;
    }

    public void addRelation(String relation) {
        String input = relation.substring(0, relation.length() - 1);
        boolean gainOrLose = input.split(" ")[2].equals("gain");
        int value = Integer.valueOf(input.split(" ")[3]);
        String relatedPersonName = input.split(" ")[input.split(" ").length - 1];
        relations.putIfAbsent(relatedPersonName, 0);
        relations.compute(relatedPersonName, (p, val) -> val = val + getValueFrom(gainOrLose, value));
    }

    private Integer getValueFrom(boolean gainOrLose, int value) {
        if (gainOrLose) {
            return value;
        }
        return -value;
    }

    public String toString() {
        return name + ": " + relations;
    }

    public String getName() {
        return name;
    }

    public String getRelations() {
        return relations.toString();
    }

}
