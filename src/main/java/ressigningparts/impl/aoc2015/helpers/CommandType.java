package ressigningparts.impl.aoc2015.helpers;

public enum CommandType {
    AND, OR, NOT, LSHIFT, RSHIFT, ASSIGN
}
