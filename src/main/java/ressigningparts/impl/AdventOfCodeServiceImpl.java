package ressigningparts.impl;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ressigningparts.ConfigurationFile;
import ressigningparts.api.AdventOfCode;
import ressigningparts.api.AdventOfCodeDTO;
import ressigningparts.api.AdventOfCodeService;

@Component("aocService")
public class AdventOfCodeServiceImpl implements AdventOfCodeService {

    @Override
    public AdventOfCodeDTO getSolutionForInput(String input, String file) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(ConfigurationFile.class);
        AdventOfCode aoc = annotationConfigApplicationContext.getBean(file, AdventOfCode.class);
        aoc.setInput(input);
        aoc.calculate();
        return new AdventOfCodeDTO(aoc.getFirstPart(), aoc.getSecondPart());
    }

    @Override
    public AdventOfCodeDTO getSolutionFormFile(String filepath) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(ConfigurationFile.class);
        AdventOfCode aoc = annotationConfigApplicationContext.getBean(filepath, AdventOfCode.class);
        aoc.setInputFromFile(filepath);
        aoc.calculate();
        return new AdventOfCodeDTO(aoc.getFirstPart(), aoc.getSecondPart());
    }
}
