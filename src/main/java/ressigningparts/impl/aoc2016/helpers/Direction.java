package ressigningparts.impl.aoc2016.helpers;

public enum Direction {
    NORTH, EAST, SOUTH, WEST, UNDEFINED;

    public boolean isHorizontal() {
        return this.equals(EAST) || this.equals(WEST);
    }

    public static Direction valueOf(char c) {
        switch (c) {
            case 'U':
                return NORTH;
            case 'L':
                return WEST;
            case 'R':
                return EAST;
            case 'D':
                return SOUTH;
            default:
                return UNDEFINED;
        }
    }

    public Direction turnLeft() {
        switch (this) {
            case NORTH:
                return WEST;
            case WEST:
                return SOUTH;
            case SOUTH:
                return EAST;
            case EAST:
                return NORTH;
            default:
                return UNDEFINED;
        }
    }

    public Direction turnRight() {
        switch (this) {
            case NORTH:
                return EAST;
            case EAST:
                return SOUTH;
            case SOUTH:
                return WEST;
            case WEST:
                return NORTH;
            default:
                return UNDEFINED;
        }
    }
}
