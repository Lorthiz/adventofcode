package ressigningparts.impl.aoc2016.helpers;

public class DirectionCommand {

    private char direction;
    private int distance;

    public DirectionCommand(String command) {
        direction = command.toCharArray()[0];
        distance = Integer.parseInt(String.valueOf(command.substring(1)));
    }

    public int getDistance() {
        return distance;
    }

    public boolean isLeft() {
        return direction == 'L';
    }
}
