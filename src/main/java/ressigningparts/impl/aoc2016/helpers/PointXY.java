package ressigningparts.impl.aoc2016.helpers;

import static java.lang.Math.abs;

public class PointXY {
    private int x, y;


    public PointXY() {
        this.x = 0;
        this.y = 0;
    }

    public PointXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public PointXY(PointXY other) {
        this.x = other.x;
        this.y = other.y;
    }

    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int value, Direction direction) {
        switch (direction) {
            case NORTH:
                y += value;
                break;
            case EAST:
                x += value;
                break;
            case SOUTH:
                y -= value;
                break;
            case WEST:
                x -= value;
                break;
            default:
                break;
        }
    }

    public void move(Direction direction) {
        switch (direction) {
            case NORTH:
                y -= 1;
                break;
            case EAST:
                x += 1;
                break;
            case SOUTH:
                y += 1;
                break;
            case WEST:
                x -= 1;
                break;
            default:
                break;
        }
    }

    public int getDistanceFromZeroPosition() {
        return abs(x) + abs(y);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof PointXY) {
            PointXY pt = (PointXY) other;
            return (x == pt.x) && (y == pt.y);
        }
        return super.equals(other);
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
