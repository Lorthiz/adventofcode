package ressigningparts.impl.aoc2016.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Triangle {

    List<Integer> lengths = new ArrayList<>();

    public Triangle(String input) {
        lengths.addAll(Arrays.asList(
                        input.split(" +")).stream()
                        .filter(s -> !s.isEmpty())
                        .map(Integer::valueOf)
                        .collect(Collectors.toList())
        );
    }

    public Triangle(Integer first, Integer second, Integer third) {
        lengths.add(first);
        lengths.add(second);
        lengths.add(third);
    }


    public String toString() {
        return "[" + getFirstValue() + ", " + getSecondValue() + ", " + getThirdValue() + "]";
    }

    public boolean isTriangle() {
        List<Integer> sortedValues = new ArrayList<>(lengths);
        sortedValues.sort(Integer::compare);
        return sortedValues.get(0) + sortedValues.get(1) > sortedValues.get(2);
    }

    public Integer getFirstValue() {
        return lengths.get(0);
    }

    public Integer getSecondValue() {
        return lengths.get(1);
    }

    public Integer getThirdValue() {
        return lengths.get(2);
    }
}
