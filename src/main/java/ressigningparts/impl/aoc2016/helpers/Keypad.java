package ressigningparts.impl.aoc2016.helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class Keypad {

    private final Map<PointXY, Integer> keyPad = new HashMap<>();
    private final PointXY fingerPosition = new PointXY();

    public Keypad(KeypadType type) {
        switch (type) {
            case SQUARE:
                configureSquare();
                break;
            case DIAMOND:
                configureDiamond();
                break;
        }
    }

    public void moveFinger(char c) {
        Direction fingerDirection = Direction.valueOf(c);
        if (!willFingerGetOutsideTheKeyPad(fingerDirection)) {
            fingerPosition.move(fingerDirection);
        }
    }

    private void configureSquare() {
        keyPad.clear();
        IntStream.range(0, 3)
                .forEach(x -> IntStream.range(0, 3)
                        .forEach(y -> {
                            Integer newValue = x + 3 * y + 1;
                            PointXY newPoint = new PointXY(x, y);
                            keyPad.put(newPoint, newValue);
                        }));
        fingerPosition.moveTo(1, 1);
    }

    private void configureDiamond() {
        keyPad.clear();

        keyPad.put(new PointXY(2, 0), 1);

        keyPad.put(new PointXY(1, 1), 2);
        keyPad.put(new PointXY(2, 1), 3);
        keyPad.put(new PointXY(3, 1), 4);

        keyPad.put(new PointXY(0, 2), 5);
        keyPad.put(new PointXY(1, 2), 6);
        keyPad.put(new PointXY(2, 2), 7);
        keyPad.put(new PointXY(3, 2), 8);
        keyPad.put(new PointXY(4, 2), 9);

        keyPad.put(new PointXY(1, 3), 10);
        keyPad.put(new PointXY(2, 3), 11);
        keyPad.put(new PointXY(3, 3), 12);

        keyPad.put(new PointXY(2, 4), 13);

        fingerPosition.moveTo(0, 2);
    }

    private boolean willFingerGetOutsideTheKeyPad(Direction fingerDirection) {
        PointXY newFingerPosition = new PointXY(fingerPosition);
        newFingerPosition.move(fingerDirection);
        return !keyPad.containsKey(newFingerPosition);
    }

    public String getNumber() {
        String number = keyPad.get(fingerPosition).toString();
        switch (number) {
            case "10":
                return "A";
            case "11":
                return "B";
            case "12":
                return "C";
            case "13":
                return "D";
            default:
                return number;
        }
    }

}
