package ressigningparts.impl.aoc2016.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.aoc2016.helpers.Keypad;
import ressigningparts.impl.aoc2016.helpers.KeypadType;
import ressigningparts.impl.AbstractAdventOfCode;

import java.util.stream.IntStream;

@Component("2016/AdventOfCode02")
public class AdventOfCode02 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        clearData();
        final Keypad keypadSquare = new Keypad(KeypadType.SQUARE);
        final Keypad keypadDiamond = new Keypad(KeypadType.DIAMOND);

        input.forEach(s -> {
            IntStream.range(0, s.toCharArray().length).forEach(i -> {
                char c = s.charAt(i);
                keypadSquare.moveFinger(c);
                keypadDiamond.moveFinger(c);
            });
            firstPart += keypadSquare.getNumber();
            secondPart += keypadDiamond.getNumber();
        });
    }

}
