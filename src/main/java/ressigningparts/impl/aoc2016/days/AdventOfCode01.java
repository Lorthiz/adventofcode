package ressigningparts.impl.aoc2016.days;

import org.springframework.stereotype.Component;
import ressigningparts.impl.aoc2016.helpers.DirectionCommand;
import ressigningparts.impl.aoc2016.helpers.Santa;
import ressigningparts.impl.AbstractAdventOfCode;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component("2016/AdventOfCode01")
public class AdventOfCode01 extends AbstractAdventOfCode {

    @Override
    public void calculate() {
        clearData();
        List<DirectionCommand> commands = parseCommandsFromInput();
        Santa santa = new Santa();
        commands.forEach(command -> executeCommand(command, santa));
        firstPart = santa.getDistanceFromDropZone();
    }

    private List<DirectionCommand> parseCommandsFromInput() {
        return Arrays.asList(input.stream().findFirst().get().split(", "))
                .stream()
                .map(DirectionCommand::new)
                .collect(Collectors.toList());
    }

    private void executeCommand(DirectionCommand command, Santa santa) {
        if (command.isLeft()) {
            santa.turnLeft();
        } else {
            santa.turnRight();
        }
        executeMovementAndSetSecondPart(command.getDistance(), santa);
    }

    private void executeMovementAndSetSecondPart(int distance, Santa santa) {
        IntStream.rangeClosed(1, distance).forEach(i -> {
            santa.moveForward();
            if (santa.amILostForTheFirstTime()) {
                secondPart = santa.getDistanceFromDropZone();
                santa.recordToFireElfResponsibleForMap();
            }
            santa.drawMap();
        });
    }
}
