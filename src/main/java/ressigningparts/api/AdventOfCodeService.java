package ressigningparts.api;

public interface AdventOfCodeService {

    AdventOfCodeDTO getSolutionForInput(String input, String file);

    AdventOfCodeDTO getSolutionFormFile(String filepath);
}
