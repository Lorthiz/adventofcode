package ressigningparts.api;

public class AdventOfCodeDTO {

    private String firstPart;
    private String secondPart;

    public AdventOfCodeDTO(String firstPart, String secondPart) {
        this.firstPart = firstPart;
        this.secondPart = secondPart;
    }

    public String getFirstPart() {
        return firstPart;
    }

    public String getSecondPart() {
        return secondPart;
    }
}
