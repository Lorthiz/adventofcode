package ressigningparts.api;

public interface AdventOfCode {
    String getFirstPart();

    String getSecondPart();

    void calculate();

    void setInputFromFile(String filePath);

    void setInput(String intput);
}
