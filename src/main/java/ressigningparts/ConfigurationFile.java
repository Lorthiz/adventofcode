package ressigningparts;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ressigningparts")
public class ConfigurationFile {

}
