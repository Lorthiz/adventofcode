package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.*;

public class AdventOfCode01Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode01();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("(())");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("()()");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void third_example_test(){
        aoc.setInput("(((");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getFirstPart());
    }

    @Test
    public void fourth_example_test(){
        aoc.setInput("(()(()(");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getFirstPart());
    }

    @Test
    public void fifth_example_test(){
        aoc.setInput("))(((((");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getFirstPart());
    }

    @Test
    public void sixth_example_test(){
        aoc.setInput("())");

        aoc.calculate();

        assertEquals(String.valueOf(-1), aoc.getFirstPart());
    }

    @Test
    public void seventh_example_test(){
        aoc.setInput("))(");

        aoc.calculate();

        assertEquals(String.valueOf(-1), aoc.getFirstPart());
    }

    @Test
    public void eighth_example_test(){
        aoc.setInput(")))");

        aoc.calculate();

        assertEquals(String.valueOf(-3), aoc.getFirstPart());
    }

    @Test
    public void ninth_example_test(){
        aoc.setInput(")())())");

        aoc.calculate();

        assertEquals(String.valueOf(-3), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void tenth_example_test(){
        aoc.setInput(")");

        aoc.calculate();

        assertEquals(String.valueOf(1), aoc.getSecondPart());
    }

    @Test
    public void eleventh_example_test(){
        aoc.setInput("()())");

        aoc.calculate();

        assertEquals(String.valueOf(5), aoc.getSecondPart());
    }

}