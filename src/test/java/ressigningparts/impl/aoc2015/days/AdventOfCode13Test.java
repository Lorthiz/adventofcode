package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

public class AdventOfCode13Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode13();
        aoc.setInputFromFile("2015/test/AdventOfCode#13");
        aoc.calculate();
    }

    @Test
    public void testGetFirstPart() throws Exception {
        System.out.println(aoc.getFirstPart());
        aoc.setInput("Mallory would lose 16 happiness units by sitting next to George.");
        aoc.calculate();

    }

    @Test
    public void testGetSecondPart() throws Exception {
        System.out.println(aoc.getFirstPart());
    }
}