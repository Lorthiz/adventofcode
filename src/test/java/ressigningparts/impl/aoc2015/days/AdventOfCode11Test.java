package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode11Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode11();
    }

    @Test
    public void first_example_test() {
        aoc.setInput("abcdefgh");

        aoc.calculate();

        assertEquals("abcdffaa", aoc.getFirstPart());
    }

    @Test
    public void second_example_test() {
        aoc.setInput("ghijklmn");

        aoc.calculate();

        assertEquals("ghjaabcc", aoc.getFirstPart());
    }
}