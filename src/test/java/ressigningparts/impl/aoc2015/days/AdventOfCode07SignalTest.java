package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.impl.aoc2015.helpers.Signal;

import static org.junit.Assert.assertEquals;

public class AdventOfCode07SignalTest {

    private Signal x, y;

    @Before
    public void setUp(){
        x = new Signal(123);
        y = new Signal(456);
    }

    @Test
    public void BitwiseAND(){
        assertEquals(72,x.AND(y).toInt());
    }

    @Test
    public void BitwiseOR(){
        assertEquals(507, x.OR(y).toInt());
    }

    @Test
    public void BitwiseLSHIFT(){
        assertEquals(492, x.LSHIFT(2).toInt());
    }

    @Test
    public void BitwiseRSHIFT(){
        assertEquals(114, y.RSHIFT(2).toInt());
    }

    @Test
    public void BitwiseNOTx(){
        assertEquals(65412, x.NOT().toInt());
    }

    @Test
    public void BitwiseNOTy(){
        assertEquals(65079, y.NOT().toInt());
    }

    @Test
    public void valueOfX(){
        assertEquals(123, x.toInt());
    }

    @Test
    public void valueOfY(){
        assertEquals(456, y.toInt());
    }
}