package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode06Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode06();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("turn on 0,0 through 999,999");

        aoc.calculate();

        assertEquals(String.valueOf(1000000), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("toggle 0,0 through 999,0");

        aoc.calculate();

        assertEquals(String.valueOf(1000), aoc.getFirstPart());
    }

    @Test
    public void third_example_test(){
        aoc.setInput("turn off 499,499 through 500,500");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void fourth_example_test(){
        aoc.setInput("turn on 0,0 through 0,0");

        aoc.calculate();

        assertEquals(String.valueOf(1), aoc.getSecondPart());
    }

    @Test
    public void fifth_example_test(){
        aoc.setInput("toggle 0,0 through 999,999");

        aoc.calculate();

        assertEquals(String.valueOf(2000000), aoc.getSecondPart());
    }

}