package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode05Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode05();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("ugknbfddgicrmopn");

        aoc.calculate();

        assertEquals(String.valueOf(1), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("aaa");

        aoc.calculate();

        assertEquals(String.valueOf(1), aoc.getFirstPart());
    }

    @Test
    public void third_example_test(){
        aoc.setInput("jchzalrnumimnmhp");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void fourth_example_test(){
        aoc.setInput("haegwjzuvuyypxyu");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void fifth_example_test(){
        aoc.setInput("dvszwmarrgswjxmb");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void sixth_example_test(){
        aoc.setInput("qjhvhtzxzqqjkmpb");

        aoc.calculate();

        assertEquals(String.valueOf(1), aoc.getSecondPart());
    }

    @Test
    public void seventh_example_test(){
        aoc.setInput("xxyxx");

        aoc.calculate();

        assertEquals(String.valueOf(1), aoc.getSecondPart());
    }

    @Test
    public void eighth_example_test(){
        aoc.setInput("uurcxstgmygtbstg");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getSecondPart());
    }

    @Test
    public void nineth_example_test(){
        aoc.setInput("ieodomkazucvgmuy");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getSecondPart());
    }

}