package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode04Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode04();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("abcdef");

        aoc.calculate();

        assertEquals(String.valueOf(609043), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("pqrstuv");

        aoc.calculate();

        assertEquals(String.valueOf(1048970), aoc.getFirstPart());
    }

}