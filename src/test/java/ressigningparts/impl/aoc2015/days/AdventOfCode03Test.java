package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode03Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode03();
    }

    @Test
    public void first_example_test(){
        aoc.setInput(">");

        aoc.calculate();

        assertEquals(String.valueOf(2), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("^>v<");

        aoc.calculate();

        assertEquals(String.valueOf(4), aoc.getFirstPart());
    }

    @Test
    public void third_example_test(){
        aoc.setInput("^v^v^v^v^v");

        aoc.calculate();

        assertEquals(String.valueOf(2), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void fourth_example_test(){
        aoc.setInput("^v");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getSecondPart());
    }

    @Test
    public void fifth_example_test(){
        aoc.setInput("^>v<");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getSecondPart());
    }

    @Test
    public void sixth_example_test(){
        aoc.setInput("^v^v^v^v^v");

        aoc.calculate();

        assertEquals(String.valueOf(11), aoc.getSecondPart());
    }

}