package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode09Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode09();
        aoc.setInputFromFile("2015/test/AdventOfCode#09");
        aoc.calculate();
    }

    @Test
    public void first_example_test(){
        assertEquals(String.valueOf(605), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void second_example_test(){
        assertEquals(String.valueOf(982), aoc.getSecondPart());
    }

}