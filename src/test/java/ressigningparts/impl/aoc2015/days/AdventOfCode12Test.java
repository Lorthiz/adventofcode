package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode12Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode12();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("[1,2,3]");

        aoc.calculate();

        assertEquals(String.valueOf(6), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("{\"a\":2,\"b\":4}");

        aoc.calculate();

        assertEquals(String.valueOf(6), aoc.getFirstPart());
    }

    @Test
    public void third_example_test(){
        aoc.setInput("[[[3]]]");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getFirstPart());
    }

    @Test
    public void fourth_example_test(){
        aoc.setInput("{\"a\":{\"b\":4},\"c\":-1}");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getFirstPart());
    }

    @Test
    public void fifth_example_test(){
        aoc.setInput("{\"a\":[-1,1]}");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void sixth_example_test(){
        aoc.setInput("[-1,{\"a\":1}]");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void seventh_example_test(){
        aoc.setInput("[]");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }

    @Test
    public void eighth_example_test(){
        aoc.setInput("{}");

        aoc.calculate();

        assertEquals(String.valueOf(0), aoc.getFirstPart());
    }
}