package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode08Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode08();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("\"\"");

        aoc.calculate();

        assertEquals(String.valueOf(2), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("\"abc\"");

        aoc.calculate();

        assertEquals(String.valueOf(2), aoc.getFirstPart());
    }

    @Test
    public void third_example_test(){
        aoc.setInput("\"aaa\\\"aaa\"");

        aoc.calculate();

        assertEquals(String.valueOf(3), aoc.getFirstPart());
    }

    @Test
    public void fourth_example_test(){
        aoc.setInput("\"\\x27\"");

        aoc.calculate();

        assertEquals(String.valueOf(5), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void fifth_example_test(){
        aoc.setInput("\"\"");

        aoc.calculate();

        assertEquals(String.valueOf(4), aoc.getSecondPart());
    }

    @Test
    public void sixth_example_test(){
        aoc.setInput("\"abc\"");

        aoc.calculate();

        assertEquals(String.valueOf(4), aoc.getSecondPart());
    }

    @Test
    public void seventh_example_test(){
        aoc.setInput("\"aaa\\\"aaa\"");

        aoc.calculate();

        assertEquals(String.valueOf(6), aoc.getSecondPart());
    }

    @Test
    public void eighth_example_test(){
        aoc.setInput("\"\\x27\"");

        aoc.calculate();

        assertEquals(String.valueOf(5), aoc.getSecondPart());
    }

    /* ADDITIONAL TESTS */

    @Test
    public void first_part_combined(){
        aoc.setInputFromFile("2015/test/AdventOfCode#08");

        aoc.calculate();

        assertEquals(String.valueOf(12), aoc.getFirstPart());
    }

    @Test
    public void second_part_combined(){
        aoc.setInputFromFile("2015/test/AdventOfCode#08");

        aoc.calculate();

        assertEquals(String.valueOf(19), aoc.getSecondPart());
    }

}