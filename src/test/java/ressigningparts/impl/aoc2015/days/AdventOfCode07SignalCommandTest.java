package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.impl.aoc2015.helpers.Signal;
import ressigningparts.impl.aoc2015.helpers.SignalCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class AdventOfCode07SignalCommandTest {

    private static HashMap<String, Signal> signals = new HashMap<>();
    private static String commandsList =
            "456 -> y\n" +
                    "x AND y -> d\n" +
                    "x OR y -> e\n" +
                    "123 -> x\n" +
                    "x LSHIFT 2 -> f\n" +
                    "y RSHIFT 2 -> g\n" +
                    "NOT x -> h\n" +
                    "NOT y -> i";
    private static ArrayList<String> commands = new ArrayList<>(Arrays.asList(commandsList.split("\n")));
    private static ArrayList<SignalCommand> ArrayOfCommands = new ArrayList<>();

    @Before
    public void setUp() {
        commands.stream().forEach(command -> ArrayOfCommands.add(new SignalCommand(command)));
        while (ArrayOfCommands.size() > 0) {
            ArrayOfCommands.removeIf(command -> command.execute(signals));
        }
    }

    @Test
    public void BitwiseAND() {
        assertEquals(72, signals.get("d").toInt());
    }

    @Test
    public void BitwiseOR() {
        assertEquals(507, signals.get("e").toInt());
    }

    @Test
    public void BitwiseLSHIFT() {
        assertEquals(492, signals.get("f").toInt());
    }

    @Test
    public void BitwiseRSHIFT() {
        assertEquals(114, signals.get("g").toInt());
    }

    @Test
    public void BitwiseNOTx() {
        assertEquals(65412, signals.get("h").toInt());
    }

    @Test
    public void BitwiseNOTy() {
        assertEquals(65079, signals.get("i").toInt());
    }

    @Test
    public void valueOfX() {
        assertEquals(123, signals.get("x").toInt());
    }

    @Test
    public void valueOfY() {
        assertEquals(456, signals.get("y").toInt());
    }

}