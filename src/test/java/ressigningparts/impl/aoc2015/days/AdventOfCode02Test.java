package ressigningparts.impl.aoc2015.days;

import org.junit.Before;
import org.junit.Test;
import ressigningparts.api.AdventOfCode;

import static org.junit.Assert.assertEquals;

public class AdventOfCode02Test {

    AdventOfCode aoc;

    @Before
    public void setUp() throws Exception {
        aoc = new AdventOfCode02();
    }

    @Test
    public void first_example_test(){
        aoc.setInput("2x3x4");

        aoc.calculate();

        assertEquals(String.valueOf(58), aoc.getFirstPart());
    }

    @Test
    public void second_example_test(){
        aoc.setInput("1x1x10");

        aoc.calculate();

        assertEquals(String.valueOf(43), aoc.getFirstPart());
    }

    /* SECOND PART */

    @Test
    public void third_example_test(){
        aoc.setInput("2x3x4");

        aoc.calculate();

        assertEquals(String.valueOf(34), aoc.getSecondPart());
    }

    @Test
    public void fourth_example_test(){
        aoc.setInput("1x1x10");

        aoc.calculate();

        assertEquals(String.valueOf(14), aoc.getSecondPart());
    }

}